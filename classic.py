from fca import Context, ManyValuedContext, Concept
# from fca.algorithms.exploration import JSMClassifier
from fca.algorithms.builders import Norris
from fca.algorithms.scaling import OrdinalGEqScaling, OrdinalLEqScaling, InterOrdinalScaling, NominalScaling, ContraNominalScaling, Scaling, IntervalScaling
from fca.algorithms.utils import Comparator
from fca.algorithms.exploration.jsm import BuildHypotheses, WeighAnObject

from utils import getLogger
import pandas as pd
from copy import deepcopy
import numpy as np

class Classic:

    def __init__(self, df, threshold, p):
        self.p = p
        self.LOGGER = getLogger("JSM_CLASSIFIER")
        self.threshold = threshold
        self.df = df.copy()
        self.df = self.df.where(pd.notnull(self.df), None)

    def _scaling(self, mvCtx):
        threshold = self.threshold
        p = self.p
        ctx1 = InterOrdinalScaling("x", mvCtx, np.linspace(p.getXRange()[0], p.getXRange()[1], 20))
        ctx2 = InterOrdinalScaling("y", mvCtx, np.linspace(p.getYRange()[0], p.getYRange()[1], 20))
        ctx3 = OrdinalGEqScaling("z", mvCtx, [threshold])
        globalCtx = ctx1.expansion_by_attribute(ctx2).expansion_by_attribute(ctx3)
        return globalCtx

    def _get_interval(self, x_setka):
        x_intervals = [] 
        for index, x in enumerate(x_setka):
            if index == len(x_setka) - 1:
                break
            x_intervals.append([x, x_setka[index + 1]])

        print(x_intervals)
        return x_intervals

    # def _scaling(self, mvCtx):
    #     threshold = self.threshold
    #     p = self.p

    #     x_intervals = self._get_interval(np.linspace(p.getXRange()[0], p.getXRange()[1], 10))
    #     y_intervals = self._get_interval(np.linspace(p.getYRange()[0], p.getYRange()[1], 10))
    #     ctx1 = IntervalScaling("x", mvCtx, x_intervals)
    #     ctx2 = IntervalScaling("y", mvCtx, y_intervals)
    #     ctx3 = OrdinalGEqScaling("z", mvCtx, [threshold])
    #     globalCtx = ctx1.expansion_by_attribute(ctx2).expansion_by_attribute(ctx3)
    #     return globalCtx

    def preprocessing(self):
        threshold = self.threshold
        p = self.p

        # Scaling
        self.LOGGER.info("Scalling...")
        mvCtx = ManyValuedContext(list(self.df.index), list(self.df.columns), self.df.values.tolist())
        globalCtx = self._scaling(mvCtx)

        df = pd.DataFrame(globalCtx._table).rename(columns={index: v for index, v in enumerate(globalCtx.attributes)}, index={index: v for index, v in enumerate(globalCtx.objects)})
        attr = "z >= {}".format(threshold)

        # Get all contexts
        self.LOGGER.info("Get pos, neg, und, glob ctxs...")
        self.posCtx = getMarkedConetxt(df, attr, True)
        self.negCtx = getMarkedConetxt(df, attr, False)
        self.undCtx = getMarkedConetxt(df, attr, None)

        # print(self.posCtx)
        df__ = df.drop([attr], axis=1)
        self.globalCtx = dfToContext(df__)

    def process(self):
        positiveContext = self.posCtx
        negativeContext = self.negCtx
        undefinedContext = self.undCtx
        globalContext = self.globalCtx

        # Support immutable
        positiveContext = deepcopy(positiveContext)
        negativeContext = deepcopy(negativeContext)
        undefinedContext = deepcopy(undefinedContext)


        self.LOGGER.info("Starting build all pos/neg concepts...")
        # Build all (includes top and bottom concepts) 
        # of Concepts in Context (reason for having positive or negative class) 
        allPositiveConcepts = Norris(positiveContext)[0]
        self.LOGGER.info(f"Positive concepts builded; Count = {len(allPositiveConcepts)}")
        allNegativeConcepts = Norris(negativeContext)[0]
        self.LOGGER.info(f"Negative concepts builded; Count = {len(allNegativeConcepts)}")

        self.LOGGER.info("Starting build pos/neg hypotheses...")
        # Build hypotheses
        positiveHypotheses = BuildHypotheses(allPositiveConcepts, allNegativeConcepts)
        self.LOGGER.info(f"Positive hypotheses builded; Count = {len(positiveHypotheses)}")
        negativeHypotheses = BuildHypotheses(allNegativeConcepts, allPositiveConcepts)
        self.LOGGER.info(f"Negative hypotheses builded; Count = {len(negativeHypotheses)}")

        # Classification
        res = []
        intents = []
        for intent in globalContext.intents():
            intents.append(intent)
            
        self.LOGGER.info("Starting classification...")
        for index, intent in enumerate(intents):
            if index % 50 == 0:
                self.LOGGER.info("Classify: {} / {}".format(index, len(intents)))

            # Compute weigh of current undefined object with intent
            positiveWeigh = WeighAnObject(positiveHypotheses, intent)
            negativeWeigh = WeighAnObject(negativeHypotheses, intent)
            
            if negativeWeigh + positiveWeigh == 0:
                res.append((globalContext.objects[index], 0))
            else:
                res.append((globalContext.objects[index], (positiveWeigh - negativeWeigh) / (positiveWeigh + negativeWeigh)))
        return res

class IntervalClassic(Classic):

    def _get_interval(self, x_setka):
        x_intervals = [] 
        for index, x in enumerate(x_setka):
            if index == len(x_setka) - 2:
                break
            x_intervals.append([x, x_setka[index + 1]])

        return x_intervals

    def _scaling(self, mvCtx):
        threshold = self.threshold
        p = self.p

        x_intervals = self._get_interval(np.linspace(p.getXRange()[0], p.getXRange()[1], 10))
        y_intervals = self._get_interval(np.linspace(p.getYRange()[0], p.getYRange()[1], 10))
        ctx1 = IntervalScaling("x", mvCtx, x_intervals)
        ctx2 = IntervalScaling("y", mvCtx, y_intervals)
        ctx3 = OrdinalGEqScaling("z", mvCtx, [threshold])
        globalCtx = ctx1.expansion_by_attribute(ctx2).expansion_by_attribute(ctx3)
        return globalCtx

def dfToContext(df_):
    return Context(list(df_.index), list(df_.columns), list(df_.values.tolist()))

def getMarkedConetxt(df, attr, mark):
    df_ = df[df[attr] == mark].drop([attr], axis=1)
    if mark is None:
        df_ = df[df[attr].isnull()].drop([attr], axis=1)
        
    return dfToContext(df_)