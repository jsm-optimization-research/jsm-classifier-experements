import logging 
import matplotlib.pyplot as plt
import os
from time import sleep
import pandas as pd
import numpy as np
import seaborn as sns
from sklearn.utils import shuffle

def getLogger(name):
    logging.debug("check")
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(u"%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)

    logger.addHandler(ch)
    
    return logger

def draw3DSurf(p, path):
    fig = plt.figure(figsize=(12,6))

    x, y, z = p.computeResponse()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    ax.plot_trisurf(x, y, z, cmap=plt.cm.viridis, linewidth=0.2)
    ax.set_title("Без шума")
    ax.view_init(30, 135)

    x, y, z = p.computeResponse(error=True)
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    ax.plot_trisurf(x, y, z, cmap=plt.cm.viridis, linewidth=0.2)
    ax.set_title("C шумом: scale={}".format(p.getErrorScale()))
    ax.view_init(30, 135)

    plt.savefig(path)
    plt.clf()

def drawHeatMap(p, path):
    fig = plt.figure(figsize=(12,6))

    x, y, z = p.computeResponse()
    df = pd.DataFrame([x,y,z]).T.rename(columns={0: "x", 1: "y", 2: "z"}).round(2)
    fig = plt.figure(figsize=(12,5))
    ax = fig.add_subplot(1, 2, 1)
    sns.heatmap(df.pivot("x","y","z"), ax=ax)
    ax.set_title("Без шума")

    x, y, z = p.computeResponse(error=True)
    df = pd.DataFrame([x,y,z]).T.rename(columns={0: "x", 1: "y", 2: "z"}).round(2)
    ax = fig.add_subplot(1, 2, 2)
    sns.heatmap(df.pivot("x","y","z"), ax=ax)
    ax.set_title("C шумом: scale={}".format(p.getErrorScale()))

    plt.savefig(path)
    plt.clf()

def shufflePoints(df, count=200):
    _df = df.copy()

    objs = shuffle(_df).head(count).index
    _df.loc[list(set(list(_df.index)).difference(objs)), "z"] = None

    return _df

def drawInitContexts(shuffleDF, markedDF, threshold, path):
    fig = plt.figure(figsize=(12, 5))

    ax = fig.add_subplot(1, 2, 1)
    sns.heatmap(shuffleDF.pivot("x","y","z"), ax=ax)

    ax = fig.add_subplot(1, 2, 2)
    sns.heatmap(markedDF.pivot("x","y","mark"), ax=ax)

    plt.savefig(path)
    plt.clf()

def drawReport(res, df, markedDF, threshold, path):
    df_ = df.copy()
    df__ = markedDF.copy()

    ROWS = 3
    COLUMNS = 2

    a = np.asarray(res)
    df_["mark"] = a[:,1]

    fig = plt.figure(figsize=(12, 5*ROWS))
    ax = fig.add_subplot(ROWS, COLUMNS, 1)
    sns.heatmap(df_.pivot("x","y","mark"), ax=ax)

    b = np.copy(a)
    b[b[:,1] > 0, 1] = 1
    b[b[:,1] < 0, 1] = -1

    df__.loc[list(df__[df__["mark"] == 0].index), "mark"] = a[np.asarray(df__[df__["mark"] == 0].index),1] 
    ax = fig.add_subplot(ROWS, COLUMNS, 2)
    sns.heatmap(df__.pivot("x","y","mark"), ax=ax)

    df__ = markedDF.copy()
    ax = fig.add_subplot(ROWS, COLUMNS, 3)
    sns.heatmap(df__.pivot("x","y","mark"), ax=ax)

    df__.loc[list(df__[df__["mark"] == 0].index), "mark"] = b[np.asarray(df__[df__["mark"] == 0].index),1] 
    ax = fig.add_subplot(ROWS, COLUMNS, 4)
    sns.heatmap(df__.pivot("x","y","mark"), ax=ax)

    ax = fig.add_subplot(ROWS, COLUMNS, 5)
    sns.heatmap(df__.pivot("x","y","mark"), ax=ax)

    df_.loc[df_["z"] >= threshold, "mark"] = 1
    df_.loc[df_["z"] < threshold, "mark"] = 0
    ax = fig.add_subplot(ROWS, COLUMNS, 6)
    sns.heatmap(df_.pivot("x","y","mark"), ax=ax)

    plt.savefig(path)
    plt.clf()

def getThreshold(_df):
    df = _df.copy()
    df = df.where(pd.notnull(df), None)
    return df[df["z"].notnull()]["z"].mean()

def addMark(_df, threshold):
    df = _df.copy()
    df = df.where(pd.notnull(df), None)
    df.loc[list(set(df.index).difference(set(list(df[df["z"].notnull()].index)))), "mark"] = 0
    df.loc[list(df[df["z"] >= threshold].index), "mark"] = 1
    df.loc[list(df[df["z"] < threshold].index), "mark"] = -1
    return df

def createDirectory(dir):
    while not os.path.exists(dir):
        os.makedirs(dir)
    