import math

def L(x, a, b):
    """Linery S member function
    
    a, b - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a < b 

    return value in range [0,1]
    """
    if x <= a:
        return 1
    if x <= b:
        return (b-x)/(b-a)
    return 0
    
def Y(x, a, b):
    """Linery Z member function
    
    a, b - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a < b 

    return value in range [0,1]
    """
    if x <= a:
        return 0
    if x <= b:
        return (x-a)/(b-a)
    return 1

def t(x, a, b, c):
    """Triangle member function
    
    a, b, c - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a <= b <= c

    return value in range [0,1]
    """
    return min(L(x,a,b), Y(x, b, c))

def T(x, a, b, c, d):
    """Trapezoid member function
    
    a, b, c, d - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a <= b <= c <= d

    return value in range [0,1]
    """
    return min(L(x,a,b), Y(x, c, d))

def Z(x, a, b):
    """Spline Z member function
    
    a, b - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a < b 

    return value in range [0,1]
    """
    if x <= a:
        return 1
    if x <= (a+b)/2:
        return 1 - 2*((x-a)/(b-a))**2
    if x <= b:
        return 2*((b-x)/(b-a))**2
    return 0

def S(x, a, b):
    """Spline S member function
    
    a, b - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a < b 

    return value in range [0,1]
    """
    if x <= a:
        return 0
    if x <= (a+b)/2:
        return 2*((x-a)/(b-a))**2
    if x <= b:
        return 1 - 2*((b-x)/(b-a))**2
    return 0

def Sigmoid(x, a, b):
    """Sigmoid member function
    
    a, b - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a < b 

    if a > 0 return S member function result
    id a < 0 return Z member function result

    return value in range [0,1]
    """
    return 1 / (1 + math.e**(-a*(x-b)))

def P1(x, a, b, c, d):
    """П member function on defaul S and Z member functions
    
    a, b, c, d - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a <= b <= c <= d

    return value in range [0,1]
    """
    return S(x,a,b) * Z(x, c,d)

def P2(x, a, b, c, d):
    """П member function on sigmoid member functions
    
    a, b, c, d - some numeric parameters taking arbitrary real values ​​and ordered by relation:
    a <= b <= c <= d

    return value in range [0,1]
    """
    return Sigmoid(x, a, b) * Sigmoid(x, c, d)

def P3(x, sigma, mean):
    """Normal distr. member function
    
    sigma**2 - dispersion
    mean - expected value

    return value in range [0,1]
    """
    return math.e**((-(x-mean)**2)/(2*sigma**2))




