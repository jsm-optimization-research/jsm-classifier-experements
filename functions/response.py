from abc import ABCMeta, abstractmethod
import scipy.stats as stats
from enum import Enum
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import numpy as np

class Goal(Enum):
    MAX = 1
    MIN = 0

class Response(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def getName(self):
        pass

    @abstractmethod
    def getXRange(self):
        pass

    @abstractmethod
    def getYRange(self):
        pass

    @abstractmethod
    def reponse(self, x, y, e):
        pass
    
    @abstractmethod
    def getGoal(self):
        pass
    
    def getErrorScale(self):
        return 0.05

    def getNormalError(self, size=(1,1)):
        scale = self.getErrorScale()
        if size == (1,1):
            return stats.norm.rvs(loc=0, scale=scale, size=(1,1), random_state=None)[0,0]
        return stats.norm.rvs(loc=0, scale=scale, size=size, random_state=None)
    
    def computeResponse(self, error=False, num=50):
        x = np.linspace(self.getXRange()[0], self.getXRange()[1], num=num) 
        y = np.linspace(self.getYRange()[0], self.getYRange()[1], num=num) 
        e = np.zeros((num,))
        _, e = np.meshgrid(e,e)
        if error:
            e = self.getNormalError(size=(num,num))
        x, y = np.meshgrid(x,y)
        z = self.reponse(x, y, e)
        return y.flatten(), x.flatten(), z.flatten()

    def saveVideo(self, file_name):
        x, y, z = self.computeResponse()
        fig = plt.figure(figsize=(6,6))
        ax = fig.gca(projection='3d')

        def init():
            ax.plot_trisurf(x, y, z, cmap=plt.cm.viridis, linewidth=0.2)
            return fig,

        def animate(i):
            ax.view_init(30, i)
            return fig,

        # Animate
        anim = animation.FuncAnimation(fig, animate, init_func=init, frames=360, interval=20, blit=True)
        anim.save(file_name + '.mp4', fps=30, extra_args=['-vcodec', 'libx264'],  progress_callback = lambda i, n: print(f'Saving frame {i} of {n}'))


class MaxiResponse(Response):

    def getGoal(self):
        return Goal.MAX

class Plato(MaxiResponse):

    def getName(self):
        return "Plato"

    def getErrorScale(self):
        return 0.5

    def getXRange(self):
        return (-3, 3)

    def getYRange(self):
        return (-3, 3)

    def reponse(self, x, y, e = None):
        return x + y + np.sin(x) + (self.getNormalError() if e is None else e)

class MountainDepression(MaxiResponse):

    def getName(self):
        return "MountainDepression"

    def getXRange(self):
        return (-2, 2)

    def getYRange(self):
        return (-2, 2)

    def reponse(self, x, y, e = None):
        return x*np.exp(-x**2-y**2) + (self.getNormalError() if e is None else e)

class Ripples(MaxiResponse):

    def getName(self):
        return "Ripples"

    def getErrorScale(self):
        return 0.1

    def getXRange(self):
        return (0, 2)

    def getYRange(self):
        return (0, 2)

    def reponse(self, x, y, e = None):
        return 10*x*np.sin(-10*x**2-10*y**2) + (self.getNormalError() if e is None else e)

class Pocky(MaxiResponse):

    def getName(self):
        return "Pocky"

    def getXRange(self):
        return (-2, 2)

    def getYRange(self):
        return (-2, 2)

    def reponse(self, x, y, e = None):
        return x*np.sin(-x**2-y**2) + (self.getNormalError() if e is None else e)

class Butterfly(MaxiResponse):

    def getName(self):
        return "Butterfly"

    def getXRange(self):
        return (-2, 2)

    def getYRange(self):
        return (-2, 2)

    def reponse(self, x, y, e = None):
        return y*np.sin(x**2)+ (self.getNormalError() if e is None else e)

# b = Ripples()
# b.drawWithoutNoise()
# print(b.reponse(0, 0))


