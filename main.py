from functions.response import Plato, Butterfly, Pocky, Ripples, MountainDepression
import pandas as pd
import numpy as np
from utils import getLogger, draw3DSurf, createDirectory, drawHeatMap, drawInitContexts, shufflePoints, addMark, getThreshold, drawReport
from datetime import datetime
from classic import Classic, IntervalClassic

DIR_SEPERATOR = '\\'
DIST_PATH = f'.{DIR_SEPERATOR}dist'
current_date = datetime.now()
LEARNING_PATH = DIST_PATH + DIR_SEPERATOR + current_date.strftime("%Y%m%d-%H%M")

RESPONSES = [
    Plato(),
    Butterfly(),
    Pocky(),
    Ripples(),
    MountainDepression()
]

LOGGER = getLogger("LEARNING")
CLASSIC_TYPE = "CLASSIC" 
FUZZY_TYPE = "FUZZY" 
JSMTYPE = CLASSIC_TYPE

for response in RESPONSES:
    RESPONSE_PATH = LEARNING_PATH + DIR_SEPERATOR + response.getName()
    LOGGER.info("Start work on {}. Path: {}".format(response.getName(), RESPONSE_PATH))
    createDirectory(RESPONSE_PATH) 

    LOGGER.info("3D surface plotting...")
    draw3DSurf(response, RESPONSE_PATH + DIR_SEPERATOR + "3DProject.png")
    LOGGER.info("Heatmap plotting...")
    drawHeatMap(response, RESPONSE_PATH + DIR_SEPERATOR + "HeatMap.png")

    x, y, z = response.computeResponse(error=True)
    df = pd.DataFrame([x,y,z]).T.rename(columns={0: "x", 1: "y", 2: "z"}).round(2)

    for percentile in np.linspace(0.02, 0.1, 5):
        PERCENTILE_PATH = RESPONSE_PATH + DIR_SEPERATOR + str(round(percentile, 2))
        createDirectory(PERCENTILE_PATH)
        count = int(df.count()[0] * percentile)
        LOGGER.info(f"Percentile: {percentile}; Count: {count}")

        shuffleDF = shufflePoints(df, count)
        threshold = getThreshold(shuffleDF)
        markedDF = addMark(shuffleDF, threshold)

        positiveCount = markedDF[markedDF["mark"] == 1].count()[0]
        negativeCount = markedDF[markedDF["mark"] == -1].count()[0]
        undefinedCount = markedDF[markedDF["mark"] == 0].count()[0]

        LOGGER.info(f"Count of positive: {positiveCount}; Count of negative: {negativeCount}; Count of undefined: {undefinedCount}")
        LOGGER.info(f"Threshold: {threshold}")
        LOGGER.info("Negative and positive examples plotting...")
        drawInitContexts(shuffleDF, markedDF, threshold, PERCENTILE_PATH + DIR_SEPERATOR + "NegativeAndPositiveExamples.png")


        LOGGER.info("Starting {JSMTYPE} classifier...")
        if JSMTYPE == CLASSIC_TYPE:
            c = Classic(shuffleDF, threshold, response) 
            c.preprocessing()
            res = c.process()
            LOGGER.info("Report plotting...")
            drawReport(res,df,markedDF, threshold, PERCENTILE_PATH + DIR_SEPERATOR + JSMTYPE + "_report.png")
            continue

        if JSMTYPE == FUZZY_TYPE:
            # Fuzzy(shuffleDF, LOGGER) 
            continue



    LOGGER.info("Finish work on {}. Path: {}".format(response.getName(), RESPONSE_PATH))
